let trainer = {
			name: 'Ash Ketchum',
			age: 10,
			friends: {
				hoenn: ['May', 'Max'],
				kanto: ['Brock', 'Misty']

			},

			pokemon: ['Pickachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
			talk: function(){
				console.log('Pickachu! I choose you!');
			},
			
		}

console.log(trainer)
console.log('Result of dot notation:')
console.log(trainer.name);
console.log('Result of square bracket notation:')
console.log(trainer['pokemon']);
console.log('Result of talk method')
trainer.talk();

let myPokemon = {
			name: 'Pikachu',
			level: [],
			health: [],
			attack: [],
			tackle: function(){
				console.log(this.name + ' tackled ' + target.name);
				console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
			
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}



		function Pokemon(name, level){
			// Properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;

			// Methods
			this.tackle = function(target){

				console.log(this.name + ' tackled ' + target.name);
				console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
				target.health = Number(target.health - this.attack);
							}

			this.faint = function(){
				console.log(this.name + ' fainted. ');
			}
		};
	let pickachu = new Pokemon('Pickachu', 12);
	let geodude = new Pokemon('Geodude', 8);
	let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pickachu);
console.log(geodude);
console.log(mewtwo);
	
geodude.tackle(pickachu);	

console.log(pickachu);

mewtwo.tackle(geodude);
geodude.faint();


console.log(geodude);
